/**
 * Light version of reactivity.
 * @MrGretty
 * 16.10.17
 */

/**
 * Function which define computed filed
 * @param {Object} obj - Object where we create a cumputed filed
 * @param {string} smartField - Name of reactive field
 * @param {Array} listFields - Reactive filed {smartField} depends on changing listFields 
 * @param {Function} cb  - Function which returns value of {smartFiled}
 */
function defineComputedField(obj, smartField, listFields, cb) {
  const startValuesOfComputedField = listFields.map(arg => obj[arg]);
  const __data = {
    ...obj,
    newObj: {},
    [smartField]: cb(...startValuesOfComputedField),
  };
  Object.keys(obj).forEach(key => {
    Object.defineProperty(obj, key, {
      enumerable: true,
      configurable: true,
      get() {
        return __data.newObj[key] || __data[key];
      },
      set(value) {
        __data.newObj[key] = value;
        if (listFields.some(prop => prop === key)) {
          const values = listFields.map(arg => obj[arg]);
          __data[smartField] = cb(...values);
        }
      },
    });
  });
  Object.defineProperty(obj, smartField, {
    get() {
      return __data[smartField];
    },
    set() {
      throw Error('error');
    },
  });
}

/**
 * Function for next level of reactivity.
 * @param {Object} obj - Return our Object. 
 * Nothing to do here(this function needs for next version of reactivity) :)  
 */
function createSmartObject(obj) {
  return obj;
}


