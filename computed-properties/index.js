function track(obj, func) {
  obj.oldTracked = obj.trackedValues.map(el => el);
  func(obj);
  obj.fn = func;
  for (let i of obj.oldTracked) {
    obj.trackedValues.shift();
  }
  obj.oldTracked = obj.trackedValues;
  console.log(obj.oldTracked);
}
function defineComputedField(obj, smartField, smartFunc) {
  const tracking = track(obj, smartFunc);
  Object.defineProperty(obj, smartField, {
    get: () => obj.res || tracking,
    set: () => {
      throw Error('error');
    },
  });
}

function createSmartObject(obj) {
  const __data = {
    ...obj,
    newObj: {},
  };
  Object.defineProperties(obj, {
    trackedValues: {
      value: [],
      enumerable: true,
      configurable: false,
      writable: true,
    },
    fn: {
      value: param => param,
      enumerable: false,
      configurable: false,
      writable: true,
    },
  });
  Object.keys(obj).forEach(key => {
    Reflect.defineProperty(obj, key, {
      enumerable: true,
      configurable: true,
      get: () => {
        obj.trackedValues.push(key);
        return __data.newObj[key] || __data[key];
      },
      set: value => {
        __data.newObj[key] = value || __data[key];
        obj.oldTracked.forEach(el => {
          if (el === key) {
            obj.res = obj.fn(obj);
          }
        });
        obj.trackedValues = [];
      },
    });
  });
  return obj;
}

const obj = createSmartObject({
  name: 'Illya',
  surname: 'Klimov',
  patronymic: 'Olegovich',
  age: 20,
});

defineComputedField(
  obj,
  'fullName',
  data => `${data.name}  ${data.surname} ${data.patronymic}`,
);
