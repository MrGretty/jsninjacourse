function search(needle, haystack) {
  return Object.values(haystack).some(el => {
    switch (typeof el) {
      case 'object':
        return search(needle, el);
      case 'undefined':
        return el === needle;
      default:
        return Number.isNaN(el) && Number.isNaN(needle)
          ? true
          : Number(el) === needle;
    }
  });
}

function IsCircled(cache, obj) {
  return cache.some(el => el === obj);
}

function equals(obj1, obj2, cache = []) {
  let isCircled = false;
  return Object.keys(obj1).length !== Object.keys(obj2).length ||
  Array.isArray(obj1) !== Array.isArray(obj2)
    ? false
    : Object.keys(obj1).every(el => {
        switch (typeof obj1[el]) {
          case 'object': {
            if (IsCircled(cache, el)) {
              if (obj1[el] === obj2[el]) {
                isCircled = true;
                break;
              } else {
                return false;
              }
            }
            cache.push(el);
            return obj1[el] === null || obj2[el] === undefined
              ? obj1[el] === obj2[el]
              : equals(obj1[el], obj2[el], cache);
          }
          case 'undefined':
            if (Object.hasOwnProperty.call(obj2, el)) {
              return obj1[el] === obj2[el];
            }
            return false;
          default:
            return Number.isNaN(obj1[el]) && Number.isNaN(obj2[el])
              ? true
              : obj1[el] === obj2[el];
        }
        if (isCircled === true) return isCircled;
      });
}

function clone(nativeObj) {
  const clonedObj = {};
  Object.keys(nativeObj).forEach(el => {
    clonedObj[el] = nativeObj[el];
  });
  return clonedObj;
}

