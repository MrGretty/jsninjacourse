
const demoData = ["Носки", "Одежда", "Животное"];

function initOptionsContainter(data) {
  const containterDiv = document.createElement('div');
  const outerDiv = document.createElement('div');

  containterDiv.classList.add('wrapper-options');
  outerDiv.classList.add('outerDiv');

  data.forEach(el => {
    const div = document.createElement('div');
    div.textContent = el;
    div.classList.add('option');
    outerDiv.appendChild(div);
  });

  containterDiv.appendChild(outerDiv);
  return containterDiv;
}

function toogleActiveClass(handler) {
  const allSiblings = document.querySelectorAll(`.${handler.target.classList[0]}`);
  allSiblings.forEach(node => {
    node.classList.remove('active');
  })
  handler.target.classList.add('active');
}


function init() {
  const select = document.querySelector('.select');
  const overlay = document.createElement('div');

  overlay.classList.add('overlay-container-backdrop');
  select.firstElementChild.textContent = "Choose..."

  const fragment = initOptionsContainter(demoData);

  select.addEventListener('click', () => {
    select.nextElementSibling.classList.add('focused');
    select.appendChild(fragment);

    document.body.appendChild(overlay);
  }, { capture: true }) //  событие на погружение 

  fragment.addEventListener('click', e => {
    select.firstElementChild.textContent = e.target.textContent;
    toogleActiveClass(e);
    select.removeChild(fragment);
    select.nextElementSibling.classList.remove('focused');

    document.body.removeChild(overlay);
  })

  overlay.addEventListener('click', () => {
    select.removeChild(fragment);
    select.nextElementSibling.classList.remove('focused');

    document.body.removeChild(overlay);

  })

}

init();