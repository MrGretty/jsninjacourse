
/* eslint-disable no-restricted-syntax */
function cached(cache, args) {
  let countAnswer = 0;
  if (Array.isArray(cache.result.args) && cache.result.args.length !== 0) {
    for (const res of cache.result.args) {
      let countArgs = 0;
      if (res.length === args.length) {
        for (const el of res) {
          if (el === args[countArgs]) countArgs += 1;
          else break;
        }
        if (countArgs === args.length) {
          const resultCache = {
            args: cache.result.args[countAnswer],
            answer: cache.result.answer[countAnswer],
          };
          return resultCache;
        }
      }
      countAnswer += 1;
    }
  }
  return false;
}
/* eslint-enable no-restricted-syntax */

function memoize(func) {
  const cache = {
    result: {
      args: [],
      answer: [],
    },
  };

  return (...args) => {
    const isCached = cached(cache, args);
    if (isCached !== false) {
      return isCached.answer;
    }
    const res = func.apply(this, args);
    cache.result.args.push(args);
    cache.result.answer.push(res);
    return res;
  };
}

function compose(...args) {
  return args.reduce((prev, current) => (...funcArgs) =>
    prev(current(...funcArgs)),
  );
}

