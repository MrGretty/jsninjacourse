class Player {
  constructor(name) {
    this.name = name;
    this.steps = [];
    this.pointerOnStep = 0;
    this.id = '';
  }
  saveSteps(point) {
    this.steps.push(point);
  }
  getAllSteps() {
    return this.steps;
  }
  getLastStep() {
    return this.steps[this.steps.length - 1 - this.pointerOnStep];
  }
  clearPointer() {
    while (this.pointerOnStep !== 0) {
      this.pointerOnStep -= 1;
    }
  }
  clearSteps() {
    this.steps = [];
  }
  setId(id) {
    this.id = id;
  }
  getId() {
    return this.id;
  }

}

module.exports = Player;
