class Storage {
  constructor(localStorage) {
    this.storage = localStorage;
  }
  static restore() {
    const storage = JSON.parse(localStorage.getItem('storage'));
    if (storage === null) {
      return new Storage({});
    }
    return new Storage(storage);
  }
  save(obj) {
    Object.defineProperty(this.storage, obj.name, {
      value: obj,
      configurable: true,
      enumerable: true,
      writable: true
    });
    localStorage.setItem('storage', JSON.stringify(this.storage));
  }
  reset() {
    this.storage = new Storage({});
    localStorage.clear();
  }
  setStorage(newStorage) {
    this.storage = newStorage;
  }
}

module.exports = Storage;
