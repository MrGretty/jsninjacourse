/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

const Player = __webpack_require__(1);
const Game = __webpack_require__(2);

function init() {
  const x = new Player('ch');
  const o = new Player('r');

  const game = new Game(x, o);
  game.startGame();

  window.addEventListener('storage', (e) => {
    console.log(e);
    game.synchronization(JSON.parse(e.newValue));
  })

}
init();



/***/ }),
/* 1 */
/***/ (function(module, exports) {

class Player {
  constructor(name) {
    this.name = name;
    this.steps = [];
    this.pointerOnStep = 0;
    this.id = '';
  }
  saveSteps(point) {
    this.steps.push(point);
  }
  getAllSteps() {
    return this.steps;
  }
  getLastStep() {
    return this.steps[this.steps.length - 1 - this.pointerOnStep];
  }
  clearPointer() {
    while (this.pointerOnStep !== 0) {
      this.pointerOnStep -= 1;
    }
  }
  clearSteps() {
    this.steps = [];
  }
  setId(id) {
    this.id = id;
  }
  getId() {
    return this.id;
  }

}

module.exports = Player;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

const Storage = __webpack_require__(3);

const battlefield = document.querySelector('.field');
const undo = document.querySelector('.undo-btn');
const redo = document.querySelector('.redo-btn');
const resultTitle = document.querySelector('.won-title');
const restartButton = document.querySelector('.restart-btn');

const cell = document.querySelectorAll('.cell');

class Game {
  constructor(playerCross, playerToe) {
    this.playerCross = playerCross;
    this.playerToe = playerToe;
    this.currentPlayer = this.playerCross;
    this.storage = Storage.restore();
    this.gameFinished = false;
  }

  render(undoBtn = false) {
    const step = document.querySelector(
      `#c-${this.currentPlayer.getLastStep()}`,
    );
    if (undoBtn) {
      step.classList.remove(this.currentPlayer.name);
    } else {
      step.classList.add(this.currentPlayer.name);
    }
  }

  stepPlayer(point) {
    if (point !== undefined) {
      this.currentPlayer.saveSteps(point);
      this.currentPlayer.setId(parseInt(point, 10));
      this.render();
    }
  }

  changePlayer() {
    return this.currentPlayer === this.playerCross
      ? this.playerToe
      : this.playerCross;
  }

  undoStep() {
    this.currentPlayer = this.changePlayer();
    this.render(true);
    this.currentPlayer.pointerOnStep += 1;
    this.storage.save(this.currentPlayer);
    Game.switchOnButton(redo);

    if (this.playerCross.pointerOnStep === this.playerCross.getAllSteps().length
      &&
      this.playerToe.pointerOnStep === this.playerToe.getAllSteps().length
    ) {
      Game.switchOffButton(undo);
    }
  }

  redoStep() {
    this.currentPlayer.pointerOnStep -= 1;
    this.render();
    this.storage.save(this.currentPlayer);
    this.currentPlayer = this.changePlayer();

    Game.switchOnButton(undo);

    if (this.playerCross.pointerOnStep + this.playerToe.pointerOnStep === 0) {
      Game.switchOffButton(redo);
    }
  }

  static switchOnButton(...buttons) {
    buttons.forEach(button => {
      if (button.hasAttribute('disabled')) {
        button.removeAttribute('disabled');
      }
    });
  }

  static switchOffButton(...buttons) {
    buttons.forEach(button => {
      if (!button.hasAttribute('disabled')) {
        button.setAttribute('disabled', '');
      }
    });
  }
  /**
   * 
   * @param {*} drawTitle Кто выиграл.
   * @param {*} drawLine  Позиции победителя, линия по позициям.
   */
  static drawResult(drawTitle, drawLine) {
    return (...args) => {
      drawTitle.call(this, args[0]);
      drawLine.call(this, args[1], args[2]);
    }
  }

  static drawResultTitle(msg) {
    resultTitle.classList.remove('hidden');
    resultTitle.firstElementChild.textContent = msg;
  }

  static drawResultWinnerLine(positions, line) {
    if (positions === null && line === null)
      return null;
    positions.forEach(el => {
      let node = document.querySelector(`#c-${el}`);
      node.classList.add('win', line);
    })
    return undefined;
  }
  //  FIXME
  static clear() {
    const chAll = document.querySelectorAll('.ch');
    const rAll = document.querySelectorAll('.r');
    resultTitle.classList.add("hidden");

    [...chAll].forEach(el => {
      const list = el.classList;
      list.remove(list[1], list[2], list[3]);
    });
    [...rAll].forEach(el => {
      const list = el.classList;
      list.remove(list[1], list[2], list[3]);
    })
  }

  static generateMatrixOfNodes(nodeCell) {
    let arr = [];
    for (let i = 0, k = 0; i < Math.sqrt(nodeCell.length); i++) {
      arr[i] = [];
      for (let j = 0; j < Math.sqrt(nodeCell.length); j++) {
        arr[i][j] = [...nodeCell][k++];
      }
    }
    return arr;
  }

  resume() {
    if (this.storage.storage.gameFinished) {
      Game.clear();
      this.storage.reset();
    } else {
      /* eslint-disable no-lonely-if */
      if (Object.keys(this.storage.storage).length !== 0) {
        Object.assign(this.playerCross, this.storage.storage.ch);
        Object.assign(this.playerToe, this.storage.storage.r);
        const pCross = this.playerCross.pointerOnStep;
        const pToe = this.playerToe.pointerOnStep;
        const resetRender = (this.playerCross.steps.length + this.playerToe.steps.length) - (pCross + pToe);
        if (resetRender > 0) {
          Game.switchOnButton(undo);
        }
        for (let i = 0; i < resetRender; i++) {
          this.render();
          this.currentPlayer.pointerOnStep += 1;
          this.currentPlayer = this.changePlayer();
        }
        this.playerCross.pointerOnStep = pCross;
        this.playerToe.pointerOnStep = pToe;

        if (this.playerCross.pointerOnStep + this.playerToe.pointerOnStep > 0) {
          Game.switchOnButton(redo);
        }
      }
    }
  }

  //  improve me
  isGameFinished() {
    const wall = Math.sqrt(cell.length);
    const matrix = Game.generateMatrixOfNodes(cell);
    const x = this.currentPlayer.getId() % wall;
    const y = parseInt(this.currentPlayer.getId() / (wall), 10);
    const winner = this.currentPlayer.name === 'ch' ? 'Crosses won!' : 'Toes won!';


    let winCombination = [];
    let findWinnerSteps = 0;
    for (let i = 0; i < wall; i++) {
      if (matrix[y][i].classList.contains(this.currentPlayer.name)) {
        findWinnerSteps++;
        winCombination.push(matrix[y][i]);
      }
    }

    if (findWinnerSteps === wall) {
      const datasetId = winCombination.map(el => el.dataset.id);
      Game.drawResult(Game.drawResultTitle, Game.drawResultWinnerLine)(winner, datasetId, 'horizontal');
      return true;
    }

    winCombination = [];
    findWinnerSteps = 0;

    for (let i = 0; i < wall; i++) {
      if (matrix[i][x].classList.contains(this.currentPlayer.name)) {
        findWinnerSteps++;
        winCombination.push(matrix[i][x]);
      }
    }

    if (findWinnerSteps === wall) {
      const datasetId = winCombination.map(el => el.dataset.id);
      Game.drawResult(Game.drawResultTitle, Game.drawResultWinnerLine)(winner, datasetId, 'vertical');
      return true;
    }

    winCombination = [];
    findWinnerSteps = 0;

    for (let i = 0; i < wall; i++) {
      if (matrix[i][i].classList.contains(this.currentPlayer.name)) {
        findWinnerSteps++;
        winCombination.push(matrix[i][i]);
      }
    }

    if (findWinnerSteps === wall) {
      const datasetId = winCombination.map(el => el.dataset.id);
      Game.drawResult(Game.drawResultTitle, Game.drawResultWinnerLine)(winner, datasetId, 'diagonal-right');
      return true;
    }

    winCombination = [];
    findWinnerSteps = 0;

    for (let i = 0; i < wall; i++) {
      if (matrix[i][wall - i - 1].classList.contains(this.currentPlayer.name)) {
        findWinnerSteps++;
        winCombination.push(matrix[i][wall - i - 1]);
      }
    }

    if (findWinnerSteps === wall) {
      const datasetId = winCombination.map(el => el.dataset.id);
      Game.drawResult(Game.drawResultTitle, Game.drawResultWinnerLine)(winner, datasetId, 'diagonal-left');
      return true;
    }

    if (this.playerCross.getAllSteps().length + this.playerToe.getAllSteps().length === cell.length) {
      Game.drawResult(Game.drawResultTitle, Game.drawResultWinnerLine)("it's a draw", null, null);
      return true;
    }
    return false;
  }

  startGame() {
    this.resume();
    battlefield.addEventListener('click', e => {
      if (!this.gameFinished) {
        this.playerToe.clearPointer();
        this.playerCross.clearPointer();
        this.stepPlayer(e.target.dataset.id);

        if (this.isGameFinished()) {
          Game.switchOffButton(undo, redo);
          this.gameFinished = true;
          this.storage.save({ name: 'gameFinished', status: this.gameFinished });
        } else {
          this.storage.save(this.currentPlayer);
          this.currentPlayer = this.changePlayer();
          Game.switchOnButton(undo);
          Game.switchOffButton(redo);
        }
      }
    });

    undo.addEventListener('click', () => {
      this.undoStep();
      this.storage.save(this.currentPlayer);
    });

    redo.addEventListener('click', () => {
      this.redoStep();
    });

    restartButton.addEventListener('click', () => {
      this.playerToe.clearPointer();
      this.playerCross.clearPointer();
      this.playerToe.clearSteps();
      this.playerCross.clearSteps();

      this.gameFinished = false;
      this.currentPlayer = this.playerCross;
      Game.clear();
      Game.switchOffButton(undo, redo);
    })

  }

  synchronization(newValue) {
    this.storage.setStorage(newValue);
    this.resume();
  }
}



module.exports = Game;


/***/ }),
/* 3 */
/***/ (function(module, exports) {

class Storage {
  constructor(localStorage) {
    this.storage = localStorage;
  }
  static restore() {
    const storage = JSON.parse(localStorage.getItem('storage'));
    if (storage === null) {
      return new Storage({});
    }
    return new Storage(storage);
  }
  save(obj) {
    Object.defineProperty(this.storage, obj.name, {
      value: obj,
      configurable: true,
      enumerable: true,
      writable: true
    });
    localStorage.setItem('storage', JSON.stringify(this.storage));
  }
  reset() {
    this.storage = new Storage({});
    localStorage.clear();
  }
  setStorage(newStorage) {
    this.storage = newStorage;
  }
}

module.exports = Storage;


/***/ })
/******/ ]);