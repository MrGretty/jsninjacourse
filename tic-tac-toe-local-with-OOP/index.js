const Player = require('./Player');
const Game = require('./Game');

function init() {
  const x = new Player('ch');
  const o = new Player('r');

  const game = new Game(x, o);
  game.startGame();

  window.addEventListener('storage', (e) => {
    console.log(e);
    game.synchronization(JSON.parse(e.newValue));
  })

}
init();

