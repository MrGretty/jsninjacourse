const cell = document.querySelectorAll('.cell');
const undoButton = document.querySelector('.undo-btn');
const redoButton = document.querySelector('.redo-btn');
const wonTitle = document.querySelector('.won-title');
const ticTacToe = document.querySelector('.field');
const restartButton = document.querySelector('.restart-btn');

/**
 *
 * @param {*} nodefileds
 */
function isAllFieldsFilled(nodefileds) {
  return [...nodefileds].every(
    node => node.classList.contains('ch') || node.classList.contains('r'),
  );
}

/**
 *
 * @param {*} rowsCount
 * @param {*} colsCount
 * @param {*} nodes
 */
function generateMatrixOfNodes(rowsCount, colsCount, nodes) {
  const arr = [];
  for (let i = 0, k = 0; i < rowsCount; i++) {
    arr[i] = [];
    for (let j = 0; j < colsCount; j++) {
      arr[i][j] = [...nodes][k++];
    }
  }
  return arr;
}

/**
 *
 * @param {*} winCombination
 * @param {*} winPosition
 * @param {*} winner
 */
function drawResult(winCombination, winPosition, winner) {
  winCombination.forEach(node => {
    node.classList.add('win', winPosition);
  });
  switchOffButton(undoButton, redoButton);
  wonTitle.classList.remove('hidden');
  wonTitle.children[0].textContent = winner;
  return true;
}

/**
 *
 * @param {*} obj
 * @param {*} selector
 * @param {*} rowsCount
 * @param {*} colsCount
 */
function isWinnerFind(obj, selector, rowsCount, colsCount) {
  const matrix = generateMatrixOfNodes(rowsCount, colsCount, cell);
  const x = obj % rowsCount;
  const y = parseInt(obj / colsCount);
  const winner = selector === 'ch' ? 'Crosses won!' : 'Toes won!';

  let winCombination = [];
  let countTrueStep = 0;
  for (let i = 0; i < rowsCount; i++) {
    if (matrix[y][i].classList.contains(selector)) {
      countTrueStep++;
      winCombination.push(matrix[y][i]);
    }
  }
  if (countTrueStep === rowsCount) {
    const datasetId = winCombination.map(el => el.dataset.id);
    collectStorage('gameOver', {
      winCombination: datasetId,
      winPosition: 'horizontal',
      winner: winner,
    });
    return drawResult(winCombination, 'horizontal', winner);
  }
  countTrueStep = 0;
  winCombination = [];

  for (let i = 0; i < colsCount; i++) {
    if (matrix[i][x].classList.contains(selector)) {
      countTrueStep++;
      winCombination.push(matrix[i][x]);
    }
  }

  if (countTrueStep === colsCount) {
    const datasetId = winCombination.map(el => el.dataset.id);
    collectStorage('gameOver', {
      winCombination: datasetId,
      winPosition: 'vertical',
      winner: winner,
    });
    return drawResult(winCombination, 'vertical', winner);
  }
  countTrueStep = 0;
  winCombination = [];

  for (let i = 0; i < colsCount; i++) {
    if (matrix[i][i].classList.contains(selector)) {
      countTrueStep++;
      winCombination.push(matrix[i][i]);
    }
  }

  if (countTrueStep === colsCount) {
    const datasetId = winCombination.map(el => el.dataset.id);
    collectStorage('gameOver', {
      winCombination: datasetId,
      winPosition: 'diagonal-right',
      winner: winner,
    });
    return drawResult(winCombination, 'diagonal-right', winner);
  }
  countTrueStep = 0;
  winCombination = [];

  for (let i = 0; i < colsCount; i++) {
    if (matrix[i][colsCount - i - 1].classList.contains(selector)) {
      countTrueStep++;
      winCombination.push(matrix[i][colsCount - i - 1]);
    }
  }

  if (countTrueStep === colsCount) {
    const datasetId = winCombination.map(el => el.dataset.id);
    collectStorage('gameOver', {
      winCombination: datasetId,
      winPosition: 'diagonal-left',
      winner: winner,
    });
    return drawResult(winCombination, 'diagonal-left', winner);
  }

  if (isAllFieldsFilled(cell)) {
    switchOffButton(undoButton, redoButton);
    wonTitle.classList.remove('hidden');
    wonTitle.children[0].textContent = "It's a draw!";
    collectStorage('gameOver', {
      winCombination: [],
      winPosition: '',
      winner: "It's a draw!",
    });
    return true;
  }
  return false;
}

/**
 *
 * @param {*} key
 * @param {*} value
 */
function step(key, value) {
  const fillFiled = document.getElementById(key);
  if (fillFiled !== null) {
    fillFiled.classList.add(value);
    return true;
  }
  return false;
}

/**
 *
 * @param {*} key
 * @param {*} value
 */
function undo(key, value) {
  document.getElementById(key).classList.remove(value);
}
/**
 *
 * @param {*} key
 * @param {*} value
 */
function redo(key, value) {
  document.getElementById(key).classList.add(value);
}

/**
 *
 * @param {*} localStorageHeap
 * @param {*} func
 */
function memoizeSteps(localStorageHeap, func) {
  const heap = localStorageHeap === null ? {} : localStorageHeap;
  return (counterUndo, ...args) => {
    let counter = counterUndo;
    while (counter !== 0) {
      const key = Object.keys(heap)[Object.keys(heap).length - counter];
      delete heap[key];
      --counter;
    }
    if (args[0] !== '') {
      Object.defineProperty(heap, args[0], {
        value: args[1],
        writable: true,
        configurable: true,
        enumerable: true,
      });
      func.apply(this, args);
      collectStorage(func.name, heap);
    }
    return heap;
  };
}

/**
 *
 * @param {*} begginer
 */
function movePointer(begginer) {
  let counter = begginer;
  return (func, storage, moveFront) => {
    let key = {};
    if (moveFront) {
      key = Object.keys(storage)[Object.keys(storage).length - 1 - counter];
      ++counter;
    } else {
      --counter;
      key = Object.keys(storage)[Object.keys(storage).length - 1 - counter];
    }
    func.call(this, key, storage[key]);
    return counter;
  };
}

/**
 *
 * @param {*} name
 * @param {*} storageStep
 */
function collectStorage(name, storageStep) {
  localStorage.setItem(name, JSON.stringify(storageStep));
}

function resumeStorage() {
  try {
    const localStorageData = JSON.parse(localStorage.step);
    const arrLocaldata = Object.keys(localStorageData);
    let gameOver = false;
    let localpointer;
    if (localStorage.pointer === undefined) {
      localpointer = 0;
    } else {
      localpointer = parseInt(localStorage.pointer);
    }
    const lastStep = Object.values(localStorageData)[
      arrLocaldata.length - localpointer - 1
    ];

    for (let i = 0; i <= arrLocaldata.length - localpointer - 1; i++) {
      const node = document.getElementById(arrLocaldata[i]);
      node.classList.add(Object.values(localStorageData)[i]);
    }

    if (localStorage.gameOver) {
      const result = JSON.parse(localStorage.gameOver);
      const nodes = result.winCombination.map(node =>
        document.getElementById(`c-${node}`),
      );
      gameOver = drawResult(nodes, result.winPosition, result.winner);
    } else if (lastStep !== undefined) {
      switchOnButton(undoButton);
    }

    if (localpointer !== 0) {
      switchOnButton(redoButton);
    }

    return [
      localStorageData,
      localpointer,
      lastStep === 'r' || lastStep === undefined ? true : false,
      gameOver,
    ];
  } catch (error) {
    console.log('data is empty', error);
    return [null, 0, true, false];
  }
}
/**
 *
 * @param {*} buttons
 */
function switchOnButton(...buttons) {
  buttons.forEach(button => {
    if (button.hasAttribute('disabled')) {
      button.removeAttribute('disabled');
    }
  });
}

function clearFields() {
  const chAll = document.querySelectorAll('.ch');
  const rAll = document.querySelectorAll('.r');
  wonTitle.classList.add('hidden');

  [...chAll].forEach(el => {
    const list = el.classList;
    list.remove(list[1], list[2], list[3]);
  });
  [...rAll].forEach(el => {
    const list = el.classList;
    list.remove(list[1], list[2], list[3]);
  });
  return true;
}

/**
 *
 * @param {*} buttons
 */
function switchOffButton(...buttons) {
  buttons.forEach(button => {
    if (!button.hasAttribute('disabled')) {
      button.setAttribute('disabled', '');
    }
  });
}

function init() {
  let storageHeap = Object.create(null);
  let counterUndoStep = 0;
  let toogleParticiant = true;
  let gameOver = false;
  [storageHeap, counterUndoStep, toogleParticiant, gameOver] = resumeStorage();

  let memoizeStep = memoizeSteps(storageHeap, step);

  const particiant = {
    0: 'ch',
    1: 'r',
  };

  let pointer = movePointer(counterUndoStep);

  ticTacToe.addEventListener('click', e => {
    if (!gameOver) {
      storageHeap = memoizeStep(
        counterUndoStep,
        e.target.id,
        particiant[+!toogleParticiant],
      );
      pointer = movePointer(0);
      counterUndoStep = 0;
      switchOnButton(undoButton);
      switchOffButton(redoButton);
      collectStorage('pointer', counterUndoStep);
      if (e.target.id !== '') {
        gameOver = isWinnerFind(
          e.target.dataset.id,
          particiant[+!toogleParticiant],
          ROWS_COUNT,
          COLS_COUNT,
        );
        if (gameOver) {
          localStorage.clear();
        } else {
          toogleParticiant = !toogleParticiant;
        }
        console.log('game finished - ', gameOver);
      }
    }
  });

  undoButton.addEventListener('click', () => {
    counterUndoStep = pointer(undo, storageHeap, true);
    if (Object.keys(storageHeap).length === counterUndoStep) {
      switchOffButton(undoButton);
    }
    switchOnButton(redoButton);
    collectStorage('pointer', counterUndoStep);
    toogleParticiant = !toogleParticiant;
  });

  redoButton.addEventListener('click', () => {
    toogleParticiant = !toogleParticiant;
    counterUndoStep = pointer(redo, storageHeap, false);
    if (counterUndoStep === 0) {
      switchOffButton(redoButton);
    }
    switchOnButton(undoButton);
    collectStorage('pointer', counterUndoStep);
  });

  restartButton.addEventListener('click', e => {
    storageHeap = Object.create(null);
    memoizeStep = memoizeSteps(null, step);
    pointer = movePointer(0);
    toogleParticiant = true;
    gameOver = false;
    counterUndoStep = 0;
    localStorage.clear();
    clearFields();
  });

  window.addEventListener('storage', e => {
    try {
      const getStorage = JSON.parse(localStorage.step);
      const lastStep = Object.keys(getStorage)[
        Object.keys(getStorage).length - 1
      ];
      if (e.key === 'step') {
        step(lastStep, getStorage[lastStep]);
        toogleParticiant = !toogleParticiant;
        switchOnButton(undoButton);
        storageHeap = getStorage;
      } else if (e.key === 'gameOver') {
        const result = JSON.parse(e.newValue);
        const nodes = result.winCombination.map(node =>
          document.getElementById(`c-${node}`),
        );
        gameOver = drawResult(nodes, result.winPosition, result.winner);
      } else {
        // e.key is pointer
        if (e.oldValue !== null) {
          if (e.oldValue < e.newValue) {
            pointer = movePointer(e.oldValue);
            counterUndoStep = pointer(undo, getStorage, true);

            if (Object.keys(getStorage).length === counterUndoStep) {
              switchOffButton(undoButton);
            }
            switchOnButton(redoButton);
          } else {
            pointer = movePointer(e.oldValue);
            counterUndoStep = pointer(redo, getStorage, false);
            if (counterUndoStep === 0) {
              switchOffButton(redoButton);
            }
            switchOnButton(undoButton);
          }
        }
      }
    } catch (e) {
      console.log('storage is empty');
      gameOver = false;
      storageHeap = Object.create(null);
      memoizeStep = memoizeSteps(null, step);
      pointer = movePointer(0);
      toogleParticiant = true;
      clearFields();
    }
  });
}

init();
