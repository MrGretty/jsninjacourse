const err = document.querySelector('.error');
const modal = document.querySelector('#modal-wrap');
const button = document.querySelector('.game-list-btn');

class Error {
    constructor(msg) {
        this.msg = msg;
    }
    show() {
        if (modal.style.display === "initial") {
            modal.style.display = "none";
            button.removeAttribute('disabled')
        }
        err.style.display = "initial";
        err.textContent = this.msg;
    }
}

module.exports = Error