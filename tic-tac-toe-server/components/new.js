const Error = require('./error');
const Ws = require('./webSocket');
const Row = require('./row');
const Cell = require('./cell');
const Render = require('./render');
const R = require('./r');
const Ch = require('./ch');

const config = require('../config/');

const render = new Render();




class Game {

    constructor() {
        this.webSocket = new Ws(config.ws);
        this.id = '';
        this.state = '';
        this.side = '';
        this.title = '';
    }

    startLoby() {
        const button = document.querySelector('.game-list-btn');
        const modal = document.querySelector('#modal-wrap');
        const gameList = document.querySelector('.simple-list');

        try {
            this.webSocket.addWsEventListeners();
        } catch (err) {
            return new Error(err).show();
        }

        button.addEventListener('click', async () => {
            button.setAttribute('disabled', true);

            try {
                this.id = await this.webSocket.newGamePost(config.newGame).then(e => e.json());
            } catch (err) {
                return new Error(`Ошибка создания игры ${err}`).show();
            }

            modal.style.display = 'initial';

            try {
                await this.webSocket.gameReadyPost(config.gameReady,
                    {
                        playerId: this.webSocket.joinId,
                        gameId: this.id.yourId
                    }
                ).then(res => res.status);
            } catch (error) {
                return new Error(`Ошибка создания игры ${error}`).show();
            }

        })

        gameList.addEventListener('click', async e => {
            try {
                await this.webSocket.gameReadyPost(config.gameReady,
                    {
                        playerId: this.webSocket.joinId,
                        gameId: e.target.textContent
                    }
                ).then(res => res.status);
            } catch (error) {
                return new Error(`Ошибка создания игры ${error}`).show();
            }
        })
        return this;
    }

    async startGame() {
        let player;
        const field = document.querySelector('.field');

        const battlefield = [];
        for (let i = 0; i < config.ROW_COUNT; i++) {
            const row = new Row();
            for (let y = 0; y < config.ROW_COUNT; y++) {
                row.add(new Cell(y + i * config.ROW_COUNT + 1))
            }
            battlefield.push(row);
        }
        render.renderGamePage(battlefield);

        const urlParams = new URLSearchParams(window.location.search);
        this.side = urlParams.get('side');

        try {
            this.state = await this.webSocket.newGameGet
                (
                config.gameGet,
                urlParams.get('gameID'),
                urlParams.get('playerID')
                )
                .then(res => res.json());

            console.log(this.state);
        } catch (error) {
            return new Error(error).show();
        }

        if (this.side === 'x') {
            player = new Ch('ch');
            this.title = 'Ваш ход';
        } else {
            player = new R('r');
            this.title = 'Ожидайте';
        }

        render.renderStepTitle(this.title);

        field.addEventListener('click', async e => {
            if (this.title === 'Ваш ход') {
                try {
                    const status = await this.webSocket.stepPost
                        (
                        config.move,
                        e.target.dataset.index,
                        urlParams.get('gameID'),
                        urlParams.get('playerID')
                        )
                        .then(res => res.status);
                    if (status === 200) {
                        render.renderStep(e.target.dataset.index, player.title);
                    }
                } catch (error) {
                    return new Error(error).show();
                }
            }
        })


        return this;
    }
}

module.exports = Game;  