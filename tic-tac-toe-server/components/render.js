
class Render {

    renderListElement(element, removed = false) {
        if (element !== null) {
            const node = document.querySelector('.simple-list');
            if (removed) {
                const removedElement = document.querySelector(`[data-id="${element.title}"]`);
                if (removedElement !== null) {
                    node.removeChild(removedElement);
                }
            } else {
                const el = document.createElement(element.name);
                el.dataset.id = element.position;
                el.textContent = element.title;
                node.appendChild(el);
            }
        }
        return this;
    }


    renderGamePage(data) {
        const field = document.querySelector('.field');
        data.forEach(dataRow => {
            const row = document.createElement('div');
            row.classList.add('row');
            dataRow.cells.forEach(dataCell => {
                const cell = document.createElement('div');
                cell.classList.add('cell');
                cell.dataset.index = dataCell.dataIndex;
                row.appendChild(cell);
            })
            field.appendChild(row);
        })
        return this;
    }

    renderStepTitle(title) {
        const infoTitle = document.querySelector('#info-title');
        infoTitle.firstElementChild.textContent = title;
        return this;
    }

    renderStep(target, player) {
        const node = document.querySelector(`[data-index="${target}"`);
        node.classList.add(player);
        return this;
    }

}

module.exports = Render