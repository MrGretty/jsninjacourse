class Row {
    constructor() {
        this.cells = [];
    }
    add(cell) {
        this.cells.push(cell);
    }
}

module.exports = Row;