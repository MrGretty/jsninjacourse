class ListItem {
    constructor() {
        this.count = 0;
        this.links = [];
    }

    /*  eslint-disable  no-plusplus */
    add(list) {
        this.links.push(list);
        this.count++;
    }

    /*  eslint-disable  no-plusplus */
    remove(name) {
        let removedIndex = 0;
        this.links.some(link => {
            removedIndex++;
            return link.title === name;
        });
        this.links.splice(removedIndex - 1);
    }

    getLinkByName(name) {
        let tempLink = null;
        this.links.forEach(link => {
            if (link.title === name)
                tempLink = link;
        })
        return tempLink;
    }
}

module.exports = ListItem