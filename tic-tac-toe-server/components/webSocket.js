const Error = require('./error');
const Render = require('./render');
const ListItem = require('./simple-list-item');
const LinkItem = require('./link-item');
const config = require('../config');

const gameList = new ListItem();
const render = new Render();

class Ws {
    constructor(connection) {
        this.connection = new WebSocket(connection);
        this.eventMessages = [];
        this.opened = false;
        this.error = '';
        this.joinId = ''
    }

    /*  eslint-disable class-methods-use-this */
    gameReadyPost(url, obj) {
        return fetch(url, {
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify(obj)
        });
    }

    //  eslint-disable-next-line
    newGamePost(url) {
        return fetch(url, { method: "post" });
    }

    stepPost(url, step, gameId, playerId) {
        return fetch(url, {
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=utf-8',
                'Game-ID': gameId,
                'Player-ID': playerId
            },
            body: JSON.stringify({ move: step })
        });
    }

    /* eslint-disable no-plusplus */
    addWsEventListeners() {
        let listCounter = 1;
        this.connection.addEventListener('message', e => {
            const parseData = JSON.parse(e.data);
            switch (parseData.action) {
                case "exception":
                    this.eventMessages.push(parseData);
                    new Error(parseData.message).show();
                    break;
                case "join":
                    this.joinId = parseData.id;
                    break;
                case "add": {
                    this.eventMessages.push(parseData);
                    const gameLink = new LinkItem(parseData.id, 'li', listCounter++);
                    gameList.add(gameLink);
                    render.renderListElement(gameLink);
                    break;
                }
                case "remove": {
                    this.eventMessages.push(parseData);
                    const removedLink = gameList.getLinkByName(parseData.id);
                    gameList.remove(removedLink);
                    render.renderListElement(removedLink, true);
                    break;
                }
                case "startGame": {
                    window.location = `${config.gamePage}?gameID=${parseData.id}&playerID=${this.joinId}&side=${parseData.side}`;
                    break;
                }
                default:
                    break;
            }
        });

        this.connection.addEventListener('open', () => {
            this.opened = true;
        });

        this.connection.addEventListener('error', e => {
            this.error = e.message;
            return new Error(this.error).show();
        })
    }

    newGameGet(url, gameId, playerId) {
        return fetch(url, {
            headers: {
                'Game-ID': gameId,
                'Player-ID': playerId
            }
        })
    }
}

module.exports = Ws

