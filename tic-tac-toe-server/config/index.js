module.exports = {
    ws: "ws://xo.t.javascript.ninja/games",
    newGame: "http://xo.t.javascript.ninja/newGame",
    gameReady: "http://xo.t.javascript.ninja/gameReady",
    gameGet: "http://xo.t.javascript.ninja/game",
    move: "http:/xo.t.javascript.ninja/move",
    ROW_COUNT: 10,
    gamePage: "game.html"
}