/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = {
    ws: "ws://xo.t.javascript.ninja/games",
    newGame: "http://xo.t.javascript.ninja/newGame",
    gameReady: "http://xo.t.javascript.ninja/gameReady",
    gameGet: "http://xo.t.javascript.ninja/game",
    move: "http:/xo.t.javascript.ninja/move",
    ROW_COUNT: 10,
    gamePage: "game.html"
}

/***/ }),
/* 1 */
/***/ (function(module, exports) {

const err = document.querySelector('.error');
const modal = document.querySelector('#modal-wrap');
const button = document.querySelector('.game-list-btn');

class Error {
    constructor(msg) {
        this.msg = msg;
    }
    show() {
        if (modal.style.display === "initial") {
            modal.style.display = "none";
            button.removeAttribute('disabled')
        }
        err.style.display = "initial";
        err.textContent = this.msg;
    }
}

module.exports = Error

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

const Game = __webpack_require__(3);
const config = __webpack_require__(0);


const game = new Game();

if (window.location.pathname === `/${config.gamePage}`) {
    game.startGame();
} else {
    game.startLoby();
}

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

const Error = __webpack_require__(1);
const Ws = __webpack_require__(4);
const Row = __webpack_require__(8);
const Cell = __webpack_require__(9);
const Render = __webpack_require__(5);
const R = __webpack_require__(10);
const Ch = __webpack_require__(11);

const config = __webpack_require__(0);

const render = new Render();




class Game {

    constructor() {
        this.webSocket = new Ws(config.ws);
        this.id = '';
        this.state = '';
        this.side = '';
        this.title = '';
    }

    startLoby() {
        const button = document.querySelector('.game-list-btn');
        const modal = document.querySelector('#modal-wrap');
        const gameList = document.querySelector('.simple-list');

        try {
            this.webSocket.addWsEventListeners();
        } catch (err) {
            return new Error(err).show();
        }

        button.addEventListener('click', async () => {
            button.setAttribute('disabled', true);

            try {
                this.id = await this.webSocket.newGamePost(config.newGame).then(e => e.json());
            } catch (err) {
                return new Error(`Ошибка создания игры ${err}`).show();
            }

            modal.style.display = 'initial';

            try {
                await this.webSocket.gameReadyPost(config.gameReady,
                    {
                        playerId: this.webSocket.joinId,
                        gameId: this.id.yourId
                    }
                ).then(res => res.status);
            } catch (error) {
                return new Error(`Ошибка создания игры ${error}`).show();
            }

        })

        gameList.addEventListener('click', async e => {
            try {
                await this.webSocket.gameReadyPost(config.gameReady,
                    {
                        playerId: this.webSocket.joinId,
                        gameId: e.target.textContent
                    }
                ).then(res => res.status);
            } catch (error) {
                return new Error(`Ошибка создания игры ${error}`).show();
            }
        })
        return this;
    }

    async startGame() {
        let player;
        const field = document.querySelector('.field');

        const battlefield = [];
        for (let i = 0; i < config.ROW_COUNT; i++) {
            const row = new Row();
            for (let y = 0; y < config.ROW_COUNT; y++) {
                row.add(new Cell(y + i * config.ROW_COUNT + 1))
            }
            battlefield.push(row);
        }
        render.renderGamePage(battlefield);

        const urlParams = new URLSearchParams(window.location.search);
        this.side = urlParams.get('side');

        try {
            this.state = await this.webSocket.newGameGet
                (
                config.gameGet,
                urlParams.get('gameID'),
                urlParams.get('playerID')
                )
                .then(res => res.json());

            console.log(this.state);
        } catch (error) {
            return new Error(error).show();
        }

        if (this.side === 'x') {
            player = new Ch('ch');
            this.title = 'Ваш ход';
        } else {
            player = new R('r');
            this.title = 'Ожидайте';
        }

        render.renderStepTitle(this.title);

        field.addEventListener('click', async e => {
            if (this.title === 'Ваш ход') {
                try {
                    const status = await this.webSocket.stepPost
                        (
                        config.move,
                        e.target.dataset.index,
                        urlParams.get('gameID'),
                        urlParams.get('playerID')
                        )
                        .then(res => res.status);
                    if (status === 200) {
                        render.renderStep(e.target.dataset.index, player.title);
                    }
                } catch (error) {
                    return new Error(error).show();
                }
            }
        })


        return this;
    }
}

module.exports = Game;  

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

const Error = __webpack_require__(1);
const Render = __webpack_require__(5);
const ListItem = __webpack_require__(6);
const LinkItem = __webpack_require__(7);
const config = __webpack_require__(0);

const gameList = new ListItem();
const render = new Render();

class Ws {
    constructor(connection) {
        this.connection = new WebSocket(connection);
        this.eventMessages = [];
        this.opened = false;
        this.error = '';
        this.joinId = ''
    }

    /*  eslint-disable class-methods-use-this */
    gameReadyPost(url, obj) {
        return fetch(url, {
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify(obj)
        });
    }

    //  eslint-disable-next-line
    newGamePost(url) {
        return fetch(url, { method: "post" });
    }

    stepPost(url, step, gameId, playerId) {
        return fetch(url, {
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=utf-8',
                'Game-ID': gameId,
                'Player-ID': playerId
            },
            body: JSON.stringify({ move: step })
        });
    }

    /* eslint-disable no-plusplus */
    addWsEventListeners() {
        let listCounter = 1;
        this.connection.addEventListener('message', e => {
            const parseData = JSON.parse(e.data);
            switch (parseData.action) {
                case "exception":
                    this.eventMessages.push(parseData);
                    new Error(parseData.message).show();
                    break;
                case "join":
                    this.joinId = parseData.id;
                    break;
                case "add": {
                    this.eventMessages.push(parseData);
                    const gameLink = new LinkItem(parseData.id, 'li', listCounter++);
                    gameList.add(gameLink);
                    render.renderListElement(gameLink);
                    break;
                }
                case "remove": {
                    this.eventMessages.push(parseData);
                    const removedLink = gameList.getLinkByName(parseData.id);
                    gameList.remove(removedLink);
                    render.renderListElement(removedLink, true);
                    break;
                }
                case "startGame": {
                    window.location = `${config.gamePage}?gameID=${parseData.id}&playerID=${this.joinId}&side=${parseData.side}`;
                    break;
                }
                default:
                    break;
            }
        });

        this.connection.addEventListener('open', () => {
            this.opened = true;
        });

        this.connection.addEventListener('error', e => {
            this.error = e.message;
            return new Error(this.error).show();
        })
    }

    newGameGet(url, gameId, playerId) {
        return fetch(url, {
            headers: {
                'Game-ID': gameId,
                'Player-ID': playerId
            }
        })
    }
}

module.exports = Ws



/***/ }),
/* 5 */
/***/ (function(module, exports) {


class Render {

    renderListElement(element, removed = false) {
        if (element !== null) {
            const node = document.querySelector('.simple-list');
            if (removed) {
                const removedElement = document.querySelector(`[data-id="${element.title}"]`);
                if (removedElement !== null) {
                    node.removeChild(removedElement);
                }
            } else {
                const el = document.createElement(element.name);
                el.dataset.id = element.position;
                el.textContent = element.title;
                node.appendChild(el);
            }
        }
        return this;
    }


    renderGamePage(data) {
        const field = document.querySelector('.field');
        data.forEach(dataRow => {
            const row = document.createElement('div');
            row.classList.add('row');
            dataRow.cells.forEach(dataCell => {
                const cell = document.createElement('div');
                cell.classList.add('cell');
                cell.dataset.index = dataCell.dataIndex;
                row.appendChild(cell);
            })
            field.appendChild(row);
        })
        return this;
    }

    renderStepTitle(title) {
        const infoTitle = document.querySelector('#info-title');
        infoTitle.firstElementChild.textContent = title;
        return this;
    }

    renderStep(target, player) {
        const node = document.querySelector(`[data-index="${target}"`);
        node.classList.add(player);
        return this;
    }

}

module.exports = Render

/***/ }),
/* 6 */
/***/ (function(module, exports) {

class ListItem {
    constructor() {
        this.count = 0;
        this.links = [];
    }

    /*  eslint-disable  no-plusplus */
    add(list) {
        this.links.push(list);
        this.count++;
    }

    /*  eslint-disable  no-plusplus */
    remove(name) {
        let removedIndex = 0;
        this.links.some(link => {
            removedIndex++;
            return link.title === name;
        });
        this.links.splice(removedIndex - 1);
    }

    getLinkByName(name) {
        let tempLink = null;
        this.links.forEach(link => {
            if (link.title === name)
                tempLink = link;
        })
        return tempLink;
    }
}

module.exports = ListItem

/***/ }),
/* 7 */
/***/ (function(module, exports) {

class LinkItem {
    constructor(title, name, position) {
        this.title = title;
        this.name = name;
        this.position = position
    }
}

module.exports = LinkItem

/***/ }),
/* 8 */
/***/ (function(module, exports) {

class Row {
    constructor() {
        this.cells = [];
    }
    add(cell) {
        this.cells.push(cell);
    }
}

module.exports = Row;

/***/ }),
/* 9 */
/***/ (function(module, exports) {

class Cell {
    constructor(dataIndex) {
        this.dataIndex = dataIndex;
    }
}

module.exports = Cell;

/***/ }),
/* 10 */
/***/ (function(module, exports) {

class R {
    consturctor(title) {
        this.title = title;
    }
}

module.exports = R;

/***/ }),
/* 11 */
/***/ (function(module, exports) {

class Ch {
    consturctor(title) {
        this.title = title;
    }
}

module.exports = Ch;

/***/ })
/******/ ]);