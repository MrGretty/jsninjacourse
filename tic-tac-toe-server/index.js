const Game = require('./components/new');
const config = require('./config');


const game = new Game();

if (window.location.pathname === `/${config.gamePage}`) {
    game.startGame();
} else {
    game.startLoby();
}