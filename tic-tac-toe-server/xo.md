## Задание 3.6. Крестики-нолики

Итак, мы начинаем писать игру крестики-нолики. По сети :) Со всеми сетевыми штуками и первое что мы должны сделать - это лобби.
Что такое лобби? Это список игр и возможность создать игру.
Выведите список игр и кнопку "Создать игру"

После загрузки страницы, вы должны установить подключение через WebSocket по адресу `ws://xo.t.javascript.ninja/games`

Через этот вебсокет Вам будут приходить уведомления следующих форматов:

* **W1** `{action: 'join', id: '345678'}` - сохранить `id`(id вашего юзера).
* **W2** `{action: 'add', id: '345678'}` - добавить в список "Существующие игры".
* **W3** `{action: 'remove', id: '345678'}` - удалить из списка "Существующие игры" игру с id ”345678". Она просто должна исчезнуть
* **W4** `{action: 'startGame', id: 'PLAYERID'}` - сервер зовет вас на игру на которую вы регистрировались командой register. (понадобится позже)
* **W5** `{action: 'exception', message: 'message'}` - вы делаете что-то не так и веб сервер сообщает Вам об этом.

При нажатии на кнопку создания игры вы должны выполнить следующие шаги:

* **C1** отключить кнопку "Создать игру" (disabled)
* **C2** отправить POST запрос на сервер по адресу `http://xo.t.javascript.ninja/newGame`, в ответ вы получите результат следующего вида: `{yourId: 'YOURID'}`
* **C3** если на предыдущем шаге возникла ошибка - вывести на странице сообщение "Ошибка создания игры" и включить кнопку "Создать игру"
* **C4** вывести на страницу сообщение "Ожидаем начала игры" (сделать видимым #modal-wrap) и отключить кнопку "Создать игру"
* **C5** отправить POST запрос с содержимым вида `{playerId: 'PLAYERID', gameId: ’GAMEID'}` по адресу `http://xo.t.javascript.ninja/gameReady`, где PLAYERID - ID полученое по вебсокету(**W1**), a GAMEID - ID игры, который вы получали при выполнение POST-запроса на `http://xo.t.javascript.ninja/newGame`
* **C6** если в ответ на POST запрос из **C5** получен код ответа `410 (Gone)` - вывести на странице сообщение "Ошибка старта игры: другой игрок не ответил"
* **C7** если запрос из **C5** провалился с кодом, отличным от `410` - вывести на странице сообщение "Неизвестная ошибка старта игры" (все ошибки должны выводиться в `.alert.error`)
* **С8** если запрос из **H0** завершился успешно, то Вам прийдет ответ от сервера: `{side: 'х'}` или `{side: 'o'}`, который говорит о том, на какой стороне вы играете

При получении команды через вебсокет на начало игры (команда `startGame`) **W4**, выполнить следующие шаги:

* **N1** отобразить поле 10x10 (каждой ячейке указать атрибут `data-index` с номером ячейки)
* **N2** отправить GET запрос о текущем состоянии игры `http://xo.t.javascript.ninja/game` c заголовками `Game-ID: GAMEID` и `Player-ID: PLAYERID` (где `GAMEID` и `РLAYERID` ровно те ID, которые получены Вами ранее), в ответ вы получите результат следующего вида: `{ reserved: { x: [], o: [] }, step: 'x' | 'o', ended: true | false, win: null | 'x' | 'o', info: { comb: [1, 2, 3], type: 'horizontal' } }`
* **N3** Показать чей ход (если Ваш ход - вывести в `#info-title` фразу 'Ваш ход', иначе - вывести в `#info-title` 'Ожидайте')
* **N4** если в ответе содержится поле `ended: true`, показать выигравшую сторону и условие выигрыша(если победил x - отобразить `'Крест выиграл!'` в `#won-title`, если победил o - отобразить `'Ноль выиграл!'` туда же), или, если поле `win: null` - показать `'Ничья'`.

* **J1** У вас есть вариант присоединения к существующей игре. Выводите список игр в списке. При клике по элементу списка - отправьте запрос **C5** - где GAMEID - ID игры, к которой вы хотите присоединиться

Перефразирую процесс подключения. Клик по "новая игра", создает "игровую комнату”. `GAMEID` - это ID игровой комнаты. Когда вы выполняете запрос `http://xo.t.javascript.ninja/gameReady` вы говорите "Сервер, я хочу учавствовать в игре с ID GAMEID". Как только таких участников двое - игра стартует, сервер присылает Вам WebSocket action `startGame`.

Игра начинается с чистого поля и хода крестика.

Ведение игры обеспечивается следующей логикой. Если сейчас Ваш ход, то при клике по ячейке вы должны выполнить следующие действия:

* **G1** отправить `POST` запрос с содержимым вида `{move: 2}` и заголовками `Game-ID: GAMEID` и `Player-ID: PLAYERID` (где `GAMEID` и `РLAYERID` ровно те ID, которые были получены Вами ранее) по адресу `http:/xo.t.javascript.ninja/move`. 2 - номер клетки, в которую выполняется ход. Нумерация от 1 до 100.

* **G2** В случае ответа 200 на **G1** поставить соответствующий крестик либо нолик по ячейке, по которой кликнули. Если в ответе сервера содержится поле win (может принимать значения 'х' либо 'о') - вывести сообщение о победе и, если в ответе есть поле info, отобразить комбинацию перечеркиванием ячеек(комбинация и ее тип содержатся в поле `info: { comb: [], type: 'Т' }`). Игра выиграна кем-то.
* **G3** В случае получения ошибки `410 (Gone)` после запроса **G2**, получить текущее состояние игры (шаг **N2**)
* **G4** В случае получения ошибки запроса **G1**, отличной от `410 (Gone)`: вывести сообщение, в котором либо содержимое поля `message` ответа, либо, если такого нет-текст "Неизвестная ошибка". Прекратить выполнение любой логики, связанной с игрой кроме кнопки новой игры

* **H0** Если сейчас же не ваш ход то вы должны выполнять Long-Polling GET запрос на адрес `http://xo.t.javascript.ninja/move`.

* **Н1** В случае провала запроса - немедленно повторить запрос (это же Long Polling)
* **Н2** В случае получения ответа - если в ответе содержится поле move - выполнить ход другого игрока в эту клетку. Если в ответе сервера содержится поле win (может принимать значения 'х' либо 'о') - вывести сообщение о победе и, если в ответе есть поле `info`, отобразить комбинацию перечеркиванием ячеек(комбинация и ее тип содержатся в поле `info: { comb: [], type: 'Combination types' }`). Игра выиграна кем-то.
* **В1** Кнопка новой игры должна иметь текст "Сдаться" если игра еще не выиграна кем-то и не произошла неизвестная ошибка. В противном случае кнопка должна иметь текст "Новая игра"
* **В2** При нажатии на кнопку "Новая игра" необходимо скрыть поле и отобразить список игр
* **ВЗ** При нажатии на кнопку "Сдаться", необходимо выполнить PUT-запрос по адресу `http://xo.t.javascript.ninja/surrender` передав такие же заголовки как в **G1**.
* **В4** При получении ответа 200 на **ВЗ** - заблокировать поле (disabled на #game-space) и отобразить список игр
* **В5** При провале запроса **ВЗ** - вывести сообщение на странице: либо содержимое поля message ответа, либо, если такого нет - текст "Неизвестная ошибка". Прекратить выполнение любой логики, связанной с текущей игрой кроме кнопки новой игры.

P.S.
Дополнительная информация:

**Classes**

Классы:

* `ch` - ячейка с символом `'x'`;
* `r` - ячейка с символом `'o'`;
* `row` - класс строки;
* `cell` - класс ячейки;
* `win` - класс ячейки, которая входит в выиграшную комбинацию;
* `error` - класс модального окна, которое содержит текст ошибки;
* `simple-list-item` и `link-item` - классы `li`-элемента на странице лобби;
* `new` - класс новой игры(которая появилась после `add` socket события);

**Combination types**

Виды выигрыша(`type` атрибут в ответе на `GET /move`, `POST /move` и `GET /game`):

* `'horizontal'`;
* `'vertical'`;
* `'diagonalRight'`;
* `'diagonalLeft'`;
